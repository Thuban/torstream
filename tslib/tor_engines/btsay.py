import bs4
import concurrent.futures

from ..utils import *

baseurl = 'https://www.btsay.org/page/'


def btsay(sch, page=0):
    sch = sch.replace(' ', '-')
    url = '{}/page/{}/'.format(baseurl,sch)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")
    reslist = []
    elements = soup.find_all('div', class_='plist')
    with concurrent.futures.ThreadPoolExecutor(max_workers=maxworkers) as executor:
        futures = [ executor.submit(btsay_parse, e) for e in elements ]
        reslist = [future.result() for future in concurrent.futures.as_completed(futures) if future.result() != None ]

    return reslist

def btsay_parse(e):
    name = str()
    magnet = str()
    size = str()
    seeds = "?"
    leechs = "?"
    res = {}

    link = e.find('div', class_='pname')
    link = link.find('a')['href']
    try:
        magnethtml = htmlget(link)
        magnetsoup = bs4.BeautifulSoup(magnethtml, 'html.parser')
        downlink = magnetsoup.find('div', class_='downlink')
        magnet = downlink.find('a')['href']
    except Exception as error:
        # deleted link
        print(error)
    pcon = e.find('div', class_='pcon')
    pcon = pcon.text.split(' ')
    pcon.pop() # remove "MB" at end
    size = pcon.pop()
    name = " ".join(pcon)

    if name and magnet and size and seeds and leechs :
        res = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": leechs}
        return res
