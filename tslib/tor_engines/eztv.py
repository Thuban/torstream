import bs4

from ..utils import *

baseurl = 'https://eztv.ag/'



def eztv(sch, page=0):
    sch = sch.replace(' ', '-')
    url = '{}/search/{}'.format(baseurl,sch)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")
    trs = soup.find_all('tr')

    name = str()
    magnet = str()
    size = str()
    seeds = str()
    leechs = str()
    res = {}
    results = []

    for tr in trs:
        tds = [ td for td in tr.find_all('td') ]
        if len(tds) == 7:
            name = tds[1].find('a', class_='epinfo')
            if name:
                name = name.string

            magnet = tds[2].find('a', class_='magnet')
            if magnet:
                magnet = magnet['href']

            size = tds[3].string

            seeds = tds[5].string
            if ',' in seeds:
                seeds = seeds.replace(',','')

            if name and magnet and size and seeds :
                res = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": '?'}
                results.append(res)
                name = str()
                magnet = str()
                size = str()
                seeds = str()

    return results
