import bs4
import concurrent.futures

from ..utils import *

baseurl = 'http://www.idope.se'

def idope(sch, page=0):
    page += 1
    url = '{}/torrent-list/{}/?p={}'.format(baseurl,sch,page)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")
    alist = soup.find_all('a')
    leechs = "?"
    reslist = []

    for div in alist:
        link = div['href']
        if link.startswith('/torrent/'):
            hash_ = link.split('/')[-2]
        else:
            continue
        name = div.find('div', class_='resultdivtopname')
        if name:
            name = name.text
        seeds = div.find('div', class_='resultdivbottonseed')
        if seeds:
            seeds = div.find('div', class_='resultdivbottonseed').text
        size = div.find('div', class_='resultdivbottonlength')
        if size:
            size = div.find('div', class_='resultdivbottonlength').text
            size = size.replace("\xa0","")
        magnet = 'magnet:?xt=urn:btih:{}'.format(hash_)

        if name and magnet and size and seeds and leechs :
            res = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": leechs}
            reslist.append(res)
        else:
            continue


    return reslist

