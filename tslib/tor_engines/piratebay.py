import bs4
import concurrent.futures

from ..utils import *

baseurl = 'https://thepiratebayz.org'



def piratebay(sch, page=0):
    """
    Return:
        List of dictionnaries with keys :
            name : torrent name
            magnet : magnet link
            size : size
            seed : number of seeds
    """
    url = '{}/search/{}///0'.format(baseurl,sch)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")

    name = str()
    magnet = str()
    size = str()
    seeds = str()
    leechs = str()
    reslist = []

    for i in soup.find_all('td'):
        if not name:
            name_result = i.find('div', class_='detName')
            if name_result:
                name = name_result.a.string

        if not magnet:
            alinks = i.find_all('a')
            for magnet_result in alinks:
                href = magnet_result['href']
                if href.startswith('magnet:'):
                    magnet = href
                    #magnet = href.split('&tr=')[0]  # to remove trackers

        if not size:
            size_result = i.find('font', class_='detDesc')
            if size_result:
                size = size_result.text.split(',')[1].replace('Size', '').strip()

        if i.string:
            if not seeds:
                seeds = i.string
            elif not leechs:
                leechs = i.string

        if name and magnet and size and seeds and leechs :

            d = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": leechs}
            name = str()
            magnet = str()
            size = str()
            seeds = str()
            leechs = str()
            reslist.append(d)
    return reslist


