import bs4
import concurrent.futures

from ..utils import *

baseurl = 'http://www.torrent9.bz'


def torrent9(sch, page=0):
    sch = sch.replace(' ', '-')
    url = '{}/search_torrent/{}.html'.format(baseurl,sch)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")
    toparse = soup.find('tbody') 
    trs = toparse.find_all('tr')

    reslist = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=maxworkers) as executor:
        futures = [ executor.submit(torrent9_parse, tr) for tr in trs]
        reslist = [future.result() for future in concurrent.futures.as_completed(futures) if future.result() != None ]

    return reslist


def torrent9_parse(tr):

    name = str()
    magnet = str()
    size = str()
    seeds = str()
    leechs = str()
    td = tr.find_all('td')
    res = {}

    name = td[0].find('a').text
    magnetlink = td[0].find('a')['href']
    size = td[1].text
    seeds = td[2].text
    leechs = td[3].text

    ms = htmlget("{}/{}".format(baseurl,magnetlink))
    magnetsoup = bs4.BeautifulSoup(ms, "html.parser")
    for m in magnetsoup.find_all('a', class_='download'):
        if m['href'].startswith('magnet:'):
            magnet = m['href']
            break

    if name and magnet and size and seeds and leechs :
        res = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": leechs}

    return res
