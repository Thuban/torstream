import subprocess
import threading
import re

# REGEX to remove colors
# https://stackoverflow.com/questions/14693701/how-can-i-remove-the-ansi-escape-sequences-from-a-string-in-python
ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
    
def dl(magnet, cmd):
    d = Downloader(cmd)
    d.start()

    return(d)


class Downloader():
    def __init__(self,cmd):
        self.cmd = cmd
        self.status = ""

    def start(self):
        thread = threading.Thread(target=self._start_cmd, args=())
        thread.daemon = True                           
        thread.start() 

    def _start_cmd(self):
        with subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True) as p:
            while True:
                line = p.stdout.readline()
                if line == '' and p.poll() is not None:
                    break
                line = line.strip()
                if len(line) > 0:
                    if len(line.replace('-','')) == 0:
                        continue
                    elif len(line.replace('=','')) == 0:
                        continue
                    elif line.startswith('FILE:'):
                        continue
                    ### remove color codes
                    self.status = ansi_escape.sub('', line)
    def get_status(self):
        return(self.status)



