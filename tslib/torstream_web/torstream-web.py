#!/usr/bin/env python3
# -*- coding:Utf-8 -*- 


"""

Auteur :      thuban <thuban@yeuxdelibad.net>  
licence :     MIT

Description : web interface to torstream

"""

import sys
import os
import random
import time
import string
import webbrowser
import mimetypes
from flask import Flask, render_template, url_for, abort, redirect, request, session, flash,jsonify 
from urllib.parse import quote as quoteurl
from urllib.parse import unquote as unquoteurl

from ..schengines import *
from ..utils import *
from ..tor_engines import *
from .download import *
from .webutils import *
from .trans import *

### torstream configuration
c = load_config()
engines = c['engines'].split(',')
# to store downloads and access them later
# contains ["magnet", "name"]
download_list = {}


### functions
def rdm_string(N):
    #https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
    chars = string.ascii_letters + string.digits + "-_+/*"
    return ''.join(random.SystemRandom().choice(chars) for _ in range(N))

def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = rdm_string(random.randrange(50,100))
    return session['_csrf_token']

def search2html(results):
    """
    convert results list into html table
    """
    html = '<table>\n'
    html += '<thead>\n'
    html += '<tr>\n'
    html += '<th class="text-left")">{}</th>\n'.format(tr_name)
    html += '<th>{}</th>\n'.format(tr_size)
    html += '<th>{}</th>\n'.format(tr_leechs)
    html += '<th id="seeds">{}</th>\n'.format(tr_seeds)
    html += '</tr>\n'
    html += '</thead>\n'

    html += '<tbody>'
    for result in results:
        # FIXME
        link = '/stream/{}'.format(quoteurl(result['magnet']))
        html += '<tr>\n'
        html += '<td><a href="{}">{}</a></td>\n'.format(link, result['name'])
        html += '<td class="text-center"><a href="{}">{}</a></td>\n'.format(link, result['size'])

        html += '<td class="text-center"><a href="{}">{}</a></td>\n'.format(link, result['leechs'])

        # color depending on seeds number
        color = 'transparent'
        if result['seeds'].isdigit():
            seeds = int(result['seeds'])
            if seeds <= 5:
                color = 'transparent'
            elif 20 >= seeds > 5:
                color = 'DarkRed'
            elif 50 >= seeds > 20:
                color = 'DarkOrange'
            elif 100 >= seeds > 50:
                color = 'ForestGreen'
            elif seeds > 100:
                color = 'DarkGreen'

        html += '<td class="text-center" style="background:{};"><a href="{}">{}</a></td>'.format(
                color, link, result['seeds'])
        html += '</tr>'
    html += '</tbody>'
    html += '</table>'

    return html



### configure flask
app = Flask(__name__)
app.secret_key = os.urandom(50)
app.jinja_env.globals['csrf_token'] = generate_csrf_token        

@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(400)

### dontpanic
@app.route('/')
def index():
    ae = ",".join([name for name in engines])
    return render_template('index.html', 
            title=title,
            welcomemsg = pick_welcomemsg(),
            logo = pick_logo(),
            avail_engines = ae,
            tr_search = tr_search,
            tr_background = tr_background,
            tr_movie_or_serie = tr_movie_or_serie,
            )


@app.route('/about')
def about():
    return render_template('about.html', 
            tr_background = tr_background,
            tr_about = tr_about)

@app.route('/ajax', methods = ['POST'])
def ajax_request():
    kw = request.form['keywords']
    engine = request.form['engine']

    html = "<h3>{} : {}</h3>".format(tr_results,engine)
    # pretty message if error
    try:
        results = search(engine, kw)
        html += search2html(results)
    except Exception as e:
        print(e)
        html += '<p>{}</p>'.format(tr_engineerror)


    return html;

@app.route('/stream/<path:magnet>')
def stream(magnet):
    magnet = unquoteurl(magnet)
    # find name of file
    names = magnet.replace('?','&')
    names = names.split('&')
    for n in names:
        if n.startswith('dn='):
            name = n.replace('dn=','')
    cmd = dlmgtcmd + [magnet]
    d = dl(magnet, cmd)
    t = 'dl{}'.format(time.time())
    download_list[t] = [d,name]
    return render_template('stream.html', 
            magnet=magnet, 
            link = t,
            tr_background = tr_background,
            cmd = ' '.join(cmd))

@app.route('/status/<dlid>')
def status(dlid):
    html = ''
    if dlid in download_list.keys():
        d = download_list[dlid][0]
        name = download_list[dlid][1]

        status = d.get_status()
        perc, speed, eta = '', '', ''
        html = status
        if '%' in status:
            perc = status.split(')')[0].split('(')[1].replace('%','')
            if 'DL:' in status and 'ETA:' in status:
                html = '<div><progress value="{}" max="100" style="min-width:200px; margin:10px; color:ForestGreen;"></progress>'.format(perc)
                html += '<br><p>{}</p></div>'.format(status)
                if int(perc) > 1:
                    html += '<div class="big"><p><a target="_blank" href="' + url_for('html5', filename=name) + '">📼 Play</a></p></div>'
    return html

@app.route('/html5/<path:filename>')
def html5(filename):
    video = find_video(os.path.join(tempdir,filename))
    if video == '':
        video = find_video(tempdir)

    mt = mimetypes.guess_type(video)[0]
    html = '<h1>{}</h1>'.format(video)
    
    if mt == 'video/x-msvideo' :
        #video = '<object data="/video/{}" width="360" height="250">\n'.format(filename)
        #video += '<param name="src" value="/video/{}" /> </object>\n'.format(filename)
        html += '<embed src="/video/{}">\n'.format(video)
    else:
        if not mt: # for None
            mtype = ""
        else:
            mtype = 'type="{}"'.format(mt)
        html += '<video width="640" controls preload="auto" autoplay="true" >\n'
        html += '<source src="/video/{}" {} >\n'.format(video, mtype)
        html += '</video>\n'

    ### FIXME if on a server
    html += '<p><a href="file://{0}" title="Direct link to video">Direct link to {1}</a></p>'.format(
            quoteurl("file://{}".format(video),video))

    return render_template('video.html', 
            tr_background = tr_background,
            html = html,
            )

# this lauch a browser
webbrowser.open('http://127.0.0.1:5000')

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
