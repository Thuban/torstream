import locale


## defaults
appname="torstream"
title="Don't panic, this is {}".format(appname)

tr_results="Results"
tr_name="Name"
tr_size="Size"
tr_leechs="Leechs"
tr_seeds="Seeds"

tr_welcome = ["What do we watch tonight?",
              "Don't forget your towel",
              "Do you have pizza?",
              "Type and watch",
              "Turn off the lights",
              "Get some drink and watch",
              "Stream and chill",
              ]

tr_search="🔎"
tr_movie_or_serie = "What do you want to watch?"
tr_background = "background"

tr_about = """
<p>This is torstream, made with ❤️  by <a href="https://yeuxdelibad.net">thuban</a></p>
<p>It let you watch movie while it's downloaded via torrent.</p>
<p>Keep in mind you are responsible of how you use torstream</p>
<ul>
<li>Licence : MIT</li>
<li><a href="https://framagit.org/Thuban/torstream">Source</a></li>

</ul>
"""
tr_engineerror = "Error while searching with this engine"

try:
    LANG=locale.getdefaultlocale()[0]
    if LANG.startswith("fr"):
        tr_results="Résultats"
        tr_name="Nom"
        tr_size="Taille"
        tr_leechs="Leechs"
        tr_seeds="Seeds"

        tr_search="🔎"
        tr_movie_or_serie = "Que voulez-vous regarder?"
        tr_background = "arrière-plan"

        tr_about = """
        <p>Voici torstream, fait avec ❤️  par <a href="https://yeuxdelibad.net">thuban</a></p>
        <p>Il vous permet de regarder des vidéos alors qu'elles sont téléchargées via torrent.</p>
        <p>Vous êtes seul responsable de ce que vous cherchez et regardez avec torstream</p>
        <ul>
            <li>Licence : MIT</li>
            <li><a href="https://framagit.org/Thuban/torstream">Source</a></li>
        </ul>
        """

        tr_welcome = ["On regarde quoi ce soir ?",
              "La pizza est chaude ?",
              "Tape et regarde",
              "On éteint les lumières",
              "Une boisson et film!",
              "Thé ✓, Couverture ✓, torstream ✓, c'est parti!",
              ]
        tr_engineerror = "Erreur lors de la recherche avec ce moteur"


except Exception:
    pass
