import random
import time
from .trans import *

def pick_welcomemsg():
    return tr_welcome[random.randrange(0, len(tr_welcome))]

def pick_logo():
    if time.strftime("%m") == "12":
        return "dontpanic-noel.png"
    else:
        return "favicon.png"


